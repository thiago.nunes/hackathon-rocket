import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { GlobalStyle } from 'config'
import * as pages from './pages'

import Modal from 'components/Modal';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/conquistas/:id" component={pages.Achievement} />
        <Route path="/preview/:code?" component={pages.PreviewCode} />
        <Route path="/dashboard" component={pages.Dashboard} />
        <Route path="/vagas" component={pages.Jobs} />
        <Redirect to="/dashboard" />
      </Switch>
      <GlobalStyle />
      <Modal />
    </Router>
  );
}

export default App;
