import styled from 'styled-components';
import { motion } from 'framer-motion';

import { colors, animation } from "config";

const asideSlide = {
  hidden: {
    x: -300,
    opacity: 0,
  },
  visible: {
    x: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
      delay: .1,
      staggerChildren: .1,
      delayChildren: .3
    }
  },
};

const itensSlide = {
  hidden: {
    y: -50,
    opacity: 0,
  },
  visible: {
    y: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
    }
  },
};

export const Wrapper = styled(motion.aside).attrs({
  initial: "hidden",
  animate: "visible",
  variants: asideSlide
})`
  display: flex;
  flex-direction: column;
  width: 240px;
  left: 0;
  top: 0;
  bottom: 0;
  position: fixed;
  background-color: ${colors.primary};
`;

export const Logo = styled(motion.img).attrs({
  variants: itensSlide
})`
  width: 130px;
  margin: 25px 15px 15px;
`;

export const Spacer = styled.div`
  flex-grow: 1;
`;

export const List = styled.nav`
  display: block;
`;

export const WrapperLink = styled(motion.div).attrs({
  variants: itensSlide
})`
  a {
    display: block;
    color: #fff;
    font-size: 16px;
    padding: 15px;
    text-decoration: none;
    transition: background-color 200ms ease-in-out;
    will-change: background-color;
    
    :hover {
      background-color: rgba(0,0,0,.1);
    }
    
    svg {
      vertical-align: middle;
      margin-right: .5em;
    }
  }
`;
