import React from 'react';
import { Link } from 'react-router-dom';
import { MdDashboard, MdGroup, MdClose } from 'react-icons/md';
import { FaTrophy } from 'react-icons/fa';
import { Wrapper, WrapperLink, List, Logo, Spacer } from './styles';
import logo from './logo.png';

function Aside() {
  return (
    <Wrapper>
      <Logo src={logo} alt="Logo do sistema" />
      <List>
        <WrapperLink>
          <Link to="/dashboard"><MdDashboard /> Início</Link>
        </WrapperLink>
        <WrapperLink>
          <Link to="/vagas"><MdGroup /> Vagas</Link>
        </WrapperLink>
      </List>
      <Spacer />
      <List>
        <WrapperLink>
          <a href="#"><MdClose /> Sair</a>
        </WrapperLink>
      </List>
    </Wrapper>
  )
}

export default Aside;
