import React from 'react';
import { Wrapper, Container } from './styles';

function Panel({
  title,
  children,
  ...props
}) {
  return (
    <Wrapper {...props}>
      {title && (
        <header>
          <h1>{title}</h1>
        </header>
      )}
      <Container>
        {children}
      </Container>
    </Wrapper>
  )
}

export default Panel;
