import styled from 'styled-components';
import { motion } from 'framer-motion';
import { animation, colors, padding } from "config";

const asideSlide = {
  hidden: {
    y: -100,
    opacity: 0,
  },
  visible: {
    y: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
    }
  },
};

export const Wrapper = styled(motion.div).attrs({
  variants: asideSlide
})`
  display: flex;
  flex: 1;
  background-color: ${colors.primary};
  border-radius: 5px;
  padding: ${padding.large}px;
  flex-direction: column;
  
  & + & {
    margin-top: ${padding.large}px;
  }
  
  header {
    padding: ${padding.large}px 0;
    font-size: 22px;
    margin: -${padding.large}px 0 ${padding.large}px;
    color: #fff;
    border-bottom: 1px solid rgba(255,255,255,.1);
    
    h1 {
      font-size: inherit;
    }
  }
`;

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
