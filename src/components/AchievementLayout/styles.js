import styled from 'styled-components'
import { colors } from "../../config";


export const Wrapper = styled.div`
  display: flex;
`;

export const Container = styled.div`
  position: fixed;
  left: 90px;
  top: 0;
  bottom: 0;
  right: 0;
  background-color: ${colors.primaryDarken};
`;

