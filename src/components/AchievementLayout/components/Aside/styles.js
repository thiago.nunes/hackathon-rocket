import styled from 'styled-components';
import { motion } from 'framer-motion';
import { animation, colors, fonts, padding } from "config";

const asideSlide = {
  hidden: {
    x: -300,
    opacity: 0,
  },
  visible: {
    x: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
      delay: .1,
      staggerChildren: .1,
      delayChildren: .1
    }
  },
};

const itensSlide = {
  hidden: {
    x: -100,
    opacity: 0,
  },
  visible: {
    x: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
    }
  },
};

export const Wrapper = styled(motion.aside).attrs({
  initial: "hidden",
  animate: "visible",
  variants: asideSlide
})`
  display: flex;
  flex-direction: column;
  width: 90px;
  left: 0;
  top: 0;
  bottom: 0;
  position: fixed;
  background-color: ${colors.primary};
  padding: ${padding.large}px 0;
  align-items: center;
`;

export const BackButton = styled(motion.button).attrs({
  variants: itensSlide
})`
  outline: none;
  border: none;
  background-color: transparent;
  cursor: pointer;
  
  transition: transform .18s;
  will-change: transform;
  
  &:hover {
    transform: scale(1.1);
  }
  
  &:active {
    transform: scale(0.9);
  }
  
  & > svg {
    font-size: ${fonts.ultra_large}px;
    color: ${colors.secondary};
  }
`;

