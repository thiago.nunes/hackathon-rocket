import React from 'react';
import { useHistory } from 'react-router-dom';
import { MdArrowBack } from "react-icons/md";
import { Wrapper, BackButton } from './styles';

function Aside() {
  const history = useHistory();

  const handleGoBack = () => {
    history.push('/dashboard');
  };

  return (
    <Wrapper>
      <BackButton onClick={handleGoBack}>
        <MdArrowBack />
      </BackButton>
    </Wrapper>
  )
}

export default Aside;
