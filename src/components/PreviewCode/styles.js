import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  position: relative;
  
  iframe {
    position: relative;
    width: 100%;
    height: 100%;
    background-color: #fff;
  }
`