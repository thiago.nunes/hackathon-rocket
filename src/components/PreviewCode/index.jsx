import React, { useState } from 'react';
import { Wrapper } from './styles';

function PreviewCode({ code }) {
  const [visible, setVisible] = useState(false);
  const encodedCode = btoa(code);

  setTimeout(() => {
    setVisible(true);
  }, 1500);

  return (
    <Wrapper>
      {
        visible && (
          <iframe src={`/preview/${code ? encodedCode : ''}`} frameBorder="0" />
        )
      }
    </Wrapper>
  )
}

export default PreviewCode;
