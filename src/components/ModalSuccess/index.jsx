import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import Button from "../Button";
import Lottie from 'react-lottie';
import animationData from './success.json'
import {colors} from "../../config";
import {useDispatch} from "react-redux";
import { Actions } from "ducks/modal";

const Wrapper = styled.div`
  width: 100%;
  max-width: 500px;
  text-align: center;
  
  h4 {
    font-size: 24px;
    color: ${colors.primary};
  }
`;

function ModalSuccess() {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    setTimeout(() => {
      dispatch(Actions.closeModal());
      history.push('/');
    }, 2000);
  }, [])

  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <Wrapper>
      <h4>Sucesso!</h4>
      <Lottie
        options={defaultOptions}
        height={250}
        width={250}
      />
    </Wrapper>
  )
}

export default ModalSuccess