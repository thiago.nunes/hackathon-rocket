import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Wrapper = styled(Link)`
  width: 100%;
  display: flex;
  text-decoration: none;
  padding: 10px;
  border-radius: 5px;
  transition: background-color 200ms ease-in-out;
  
  :hover {
    background-color: rgba(0,0,0,.1);
  }
  
  & + & {
    margin-top: 5px;
  }
`;

export const TextWrapper = styled.div`
  display: flex;
  margin-left: 15px;
  justify-content: center;
  align-items: flex-start;
  color: #fff;
  flex-direction: column;
  
  h2 { font-size: 18px }
  p { 
    font-size: 16px; 
    color: rgba(255,255,255,.5);
    margin-top: 5px; 
  }
`;