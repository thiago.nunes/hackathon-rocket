import React from 'react';
import Avatar from "../Avatar";
import { Wrapper, TextWrapper } from './styles';

function ListAchievement({ data }) {
  if (!data) return null;

  return data.map(({ id, image, title, text }) => (
    <Wrapper to={`/conquistas/${id}`}>
      <Avatar size={60} src={image} alt={title} />
      <TextWrapper>
        <h2>{title}</h2>
        <p>{text}</p>
      </TextWrapper>
    </Wrapper>
  ))
}

export default ListAchievement;