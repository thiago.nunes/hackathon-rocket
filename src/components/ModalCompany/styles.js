import styled from 'styled-components';
import { colors, fonts } from "../../config";

export const Wrapper = styled.div`
  width: 100%;
  max-width: 500px;
  
`;

export const Title = styled.h2`
  font-size: ${fonts.ultra_large};
  color: ${colors.primary};
`;

export const PriceRange = styled.div`
  color: rgba(0,0,0,.8);
  margin-bottom: 1em;
`;

export const Description = styled.p`
  color: rgba(0,0,0,.8);
  margin-bottom: 10px;
  
  p {
  margin-bottom: .5em;
  }
  
  strong {
    font-weight: 700;
  }
  
  ul {
    margin-left: 25px;
  }
`;

export const Footer = styled.footer`
  padding-top: 30px;
  display: flex;
  position: relative;
`;

export const Spacer = styled.div`
  flex-grow: 1;
`
