import React from 'react';
import Badge from 'components/Badge';
import Button from 'components/Button';
import { Footer, Spacer, Wrapper, Title, PriceRange, Description } from './styles';

// Services
import * as userService from 'services/user';

function ModalCompany({ handleClose, job: { user_id, opportunity_id, title, priceRange, description, descriptionHTML, skills } }) {
  const onClickApply = () => {
    userService.applyJob(user_id, opportunity_id);
  };

  return (
    <Wrapper>
      <Title>{title}</Title>
      <PriceRange>{priceRange}</PriceRange>
      <Description dangerouslySetInnerHTML={{ __html: descriptionHTML }} />
      {skills && skills.map((skill) => (
        <Badge fontSize={"ultra_small"} padding={"ultra_small"} key={skill}>{skill}</Badge>
      ))}
      <Footer>
        <Spacer />
        <Button onClick={onClickApply}>Me candidatar</Button>
      </Footer>
    </Wrapper>
  )
}

export default ModalCompany;
