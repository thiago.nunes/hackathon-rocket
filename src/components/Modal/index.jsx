import React from 'react';
import { MdClose } from "react-icons/md";
import { AnimatePresence } from 'framer-motion';
import { useDispatch, useSelector } from "react-redux";

// Styles
import {
  Overlay,
  Content,
  Wrapper,
  CloseButton
} from './styles';
import { Actions } from "ducks/modal";

export default function Modal() {
  const dispatch = useDispatch();

  const handleClose = () => dispatch(Actions.closeModal());

  const visible = useSelector(state => state.modal.visible);
  const Component = useSelector(state => state.modal.component);


  // if(!visible) return null;

  return (
    <AnimatePresence>
      {visible && (
        <Overlay onClick={handleClose}>
          <Wrapper>
            <Content onClick={e => e.stopPropagation()}>
              <CloseButton onClick={handleClose}>
                <MdClose />
              </CloseButton>
              <Component handleClose={handleClose} />
            </Content>
          </Wrapper>
        </Overlay>
      )}
    </AnimatePresence>
  );
};
