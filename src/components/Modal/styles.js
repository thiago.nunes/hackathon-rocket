import styled from 'styled-components';
import { motion } from 'framer-motion';
import { colors, padding } from "../../config";

const animationOverlay = {
  hidden: {
    opacity: 0,
    transition: {
      duration: .1
    }
  },
  visible: {
    opacity: 1,
    transition: {
      duration: .1,
      delayChildren: .18
    }
  }
};

const animationModal = {
  hidden: {
    opacity: 0,
    y: -25,
    transition: {
      duration: .3
    }
  },
  visible: {
    opacity: 1,
    y: 0,
    transition: {
      duration: .3
    }
  }
};

export const Overlay = styled(motion.div).attrs({
  initial: "hidden",
  animate: "visible",
  exit: "hidden",
  variants: animationOverlay
})`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: rgba(0, 0, 0, .5);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled(motion.div).attrs({
  variants: animationModal
})`
  display: grid;
  align-items: center;
  height: 100%;
`;

export const Content = styled.div`
  border-radius: 5px;
  display: flex;
  background-color: #fff;
  padding: ${padding.large}px;
  position: relative;
`;

export const CloseButton = styled.button`
  cursor: pointer;
  border-radius: 100%;
  width: 25px;
  height: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  outline: none;
  background-color: ${colors.primary};
  color: ${colors.secondary};
  position: absolute;
  top: 0;
  right: 0;
  transform: translate(50%, -50%);
`;
