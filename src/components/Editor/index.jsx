import React, { useState } from 'react';
import { MdKeyboardArrowRight, MdHelp } from 'react-icons/md';
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/theme-monokai";

// Styles
import { Wrapper, BoxLeft, HelpText, Divider, Options } from './styles';

// Components
import PreviewCode from 'components/PreviewCode';
import Button from 'components/Button';

function Editor({ initialValue, onSuccess, onClickHelp }) {
  const [value, setValue] = useState(initialValue);
  const [code, setCode] = useState(initialValue);

  const handleClickCode = () => {
    onSuccess();
    setCode(value);
  };

  return (
    <Wrapper>
      <BoxLeft>
        <AceEditor
          width="100%"
          height="100%"
          mode="html"
          theme="monokai"
          name="UNIQUE_ID"
          onChange={(newValue) => {
            setValue(newValue);
          }}
          value={value || ''}
          setOptions={{
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            enableSnippets: true,
            showLineNumbers: true,
            tabSize: 2,
          }}
        />
        <HelpText>
          <span>Ajude o batman a encontrar o robin</span>
          <Options>
            <Button onClick={onClickHelp} color={'secondary'}>Ajuda <MdHelp /></Button>
            <Button onClick={handleClickCode} color={'secondary'}>
              Testar <MdKeyboardArrowRight />
            </Button>
          </Options>
        </HelpText>
      </BoxLeft>
      <Divider />
      <PreviewCode code={code} />
    </Wrapper>
  )
}

Editor.defaultProps = {
  initialValue: null,
}

export default Editor;
