import styled from 'styled-components';
import { motion } from 'framer-motion'
import { animation, colors } from "../../config";

const slideAnimation = {
  hidden: {
    x: -300,
    opacity: 0,
  },
  visible: {
    x: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
      delay: .1,
      staggerChildren: .1,
      delayChildren: .3
    }
  },
};

const itemAnimation = {
  hidden: {
    y: 100,
    opacity: 0,
  },
  visible: {
    y: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
    }
  },
};

export const Wrapper = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  height: 100%;
`;

export const BoxLeft = styled(motion.div).attrs({
  initial: "hidden",
  animate: "visible",
  variants: slideAnimation
})`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 100%;
  flex-grow: 1;
`;

export const HelpText = styled(motion.div).attrs({
  variants: itemAnimation
})`
  width: 100%;
  line-height: 85px;
  padding-left: 30px;
  background-color: #fff;
  min-height: 0;
  display: flex;
  justify-content: space-between;
  border-right: 1px solid #DEDEDE;
  
  span {
    color: ${colors.primary};
  }
  
  button {
    height: 85px;
    padding-left: 15px;
    padding-right: 15px;
    cursor: pointer;
    background-color: transparent;
    
    :hover {
      background-color: rgba(0,0,0,.1);
    }
  }
  
  svg {
    vertical-align: middle;
  }
`

export const Divider = styled.div`
  width: 20px;
  position: relative;
  background-color: ${colors.primary};
  
  &:after {
    content: "";
    width: 3px;
    border-radius: 50px;
    height: 50px;
    background-color: #fff;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const Options = styled.div`
  display: flex;
`
