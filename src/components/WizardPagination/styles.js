import styled from 'styled-components';

export const Wrapper = styled.div`
  min-width: 300px;
`;

export const Footer = styled.div`
  display: flex;
  padding-top: 25px;
  justify-content: space-between;
`;

export const WrapperPaginator = styled.div`
  display: flex;
 
`;
