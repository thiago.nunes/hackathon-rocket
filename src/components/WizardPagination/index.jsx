import React, { useMemo, useEffect, useState } from 'react';
import { MdArrowBack, MdArrowForward, MdDone } from 'react-icons/md';

// Components
import Button from 'components/Button';

// Styles
import {
  Wrapper,
  WrapperPaginator,
  Footer,
} from './styles';

export default function WizardPagination({
  pages,
  index,
  handleFinalStepClick,
  ...props
}) {
  const [_index, _setIndex] = useState(index || 0);

  useEffect(() => {
    if(/\d/g.test(index) && index < pages.length && index >= 0) {
      _setIndex(index);
    }
  }, [index]);

  const CurrentPage = useMemo(() => {
    return pages[_index]
  }, [pages, _index]);

  const handleNavigate = (i) => {
    if(i < pages.length && i >= 0) {
      console.log(pages.length);
      console.log(i);

      _setIndex(i);
    }
  };

  return (
    <Wrapper>
      <CurrentPage />
      <Footer>
          <WrapperPaginator>
            {_index > 0 && (
              <Button onClick={() => handleNavigate(_index - 1)}>
                <MdArrowBack style={{ marginRight: 8 }}/> Anterior
              </Button>
            )}
          </WrapperPaginator>
          {_index < (pages.length - 1) && (
            <WrapperPaginator>
              <Button onClick={() => handleNavigate(_index + 1)}>
                Próximo <MdArrowForward style={{ marginLeft: 8 }} />
              </Button>
            </WrapperPaginator>
          )}
          {_index === (pages.length - 1) && handleFinalStepClick && (
            <WrapperPaginator>
              <Button onClick={handleFinalStepClick} color={"success"}>
                Ok, bora codar <MdDone style={{ marginLeft: 8 }} />
              </Button>
            </WrapperPaginator>
          )}
      </Footer>
    </Wrapper>
  )
}
