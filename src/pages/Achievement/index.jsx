import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";
import AchievementLayout from 'components/AchievementLayout';
import Editor from 'components/Editor';
import { Actions } from 'ducks/modal';
import WizardPagination from 'components/WizardPagination';
import ModalSuccess from 'components/ModalSuccess';
import { WrapperContent } from './styles';

export default function Achievement() {
  const dispatch = useDispatch();

  const openModalSuccess = () => {
    dispatch(Actions.openModal(() => (
      <ModalSuccess />
    )))
  }

  const openModalWizard = () => {
    dispatch(Actions.openModal(() => (
      <WrapperContent>
        <WizardPagination
          handleFinalStepClick={() => dispatch(Actions.closeModal())}
          pages={[
            () => (
              <div>
                <h4>Bem vindo a Gotham City caro viajante, nos deparamos com mais um desafio! (IF-ELSE)</h4>
                <p>
                  Você chegou em um momento crítico! <br />
                  Precisamos da sua ajuda, pois nosso herói Batman está ausente. Você tem que tomar uma decisão agora! <br /> <br />
                  SE você nos ajudar teremos uma esperança de combater o mal <br />
                  SENÃO nossa cidade estará perdida!
                </p>
              </div>
            ),
            () => (
              <div>
                <h4>Exemplo</h4>
<pre dangerouslySetInnerHTML={{ __html: `const queroAjudar = true; // VERDADEIRO

if (queroAjudar) {
  alert('Gotham está a salva');
} else {
  alert('Gotham foi dominada pelo mal');
}` }} />
                Resultado: Gotham está a salva
              </div>
            ),
            () => <div>
              <h4>Exemplo</h4>
              <pre dangerouslySetInnerHTML={{ __html: `const queroAjudar = false; // FALSO
              
IF (queroAjudar) {
	alert('Gotham está a salva');
} ELSE {
	alert('Gotham foi dominada pelo mal');
}` }} />
              Resultado: Gotham foi dominada pelo mal
            </div>,

            () => <div>
              <div>
                <h4>Agora vamos codar na prática</h4>
                <p>
                  Vamos botar o nosso exemplo na prática <br />
                  Crie a condicional para salvar a cidade usando a variável "queroAjudar"<br /> <br />
                  SE a variável "queroAjudar" for verdadeira, execute um alert dizendo "Quero AJUDAR!"<br />
                  SENÃO a variável "queroAjudar" for false, execute um alert dizendo "Não to afim de ajudar."
                </p>
              </div>
            </div>
          ]}
        />
      </WrapperContent>
    )))
  };


  useEffect(() => {
    openModalWizard();
  }, []);


  return (
    <AchievementLayout>
      <Editor onSuccess={openModalSuccess} initialValue={`<script>
// Bora CODAR
let queroAjudar = true;

</script>

<!-- Não é necessário alterar essa parte --> 

<style>
  .body { 
    background: linear-gradient(to bottom, rgba(0, 0, 0, 1), rgba(0, 0, 0, .85), rgba(0, 0, 0, 1));
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
  }
</style>
<div class="body">
  <img width="300px" height="auto" src="https://3.bp.blogspot.com/-Tnpk46I1aPs/V4xnode11sI/AAAAAAAAKtY/IJznizfQAecvMP-kQBMbYSYVG1ICRvvFQCLcB/s1600/escudo-do-batman-em-png-vetorizado-queroimagem-cei%25C3%25A7a-crispim.png" />
</div>`} onClickHelp={openModalWizard} />
    </AchievementLayout>
  )
}
