import styled from 'styled-components';
import { colors } from "../../config";

export const WrapperContent = styled.div`
  color: ${colors.primary};
  max-width: 550px;
  
  h4 {
  margin-bottom: .5em;
  }
  
  pre {
    padding: 25px 0;
  }
`;
