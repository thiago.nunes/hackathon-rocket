import React from 'react';
import { useSelector } from "react-redux";

// Components
import Panel from "components/Panel";

// Styles
import {
  Quantity,
  QuantityTitle,
  Section,
  FilterTitle,
  Divider,
} from './styles';

export default function Filter({
  ...props
}) {
  const jobs = useSelector(state => state.job.data);

  return (
    <Panel>
      <Section>
        <Quantity>{jobs.length}</Quantity>
        <QuantityTitle>VAGA(S) DISPONÍVEL(IS)</QuantityTitle>
      </Section>
    </Panel>
  )
}
