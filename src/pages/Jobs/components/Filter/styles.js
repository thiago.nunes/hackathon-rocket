import styled from 'styled-components';
import { colors, fonts, padding } from "config";

export const Section = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`;

export const Quantity = styled.span`
  width: 100%;
  color: ${colors.secondary};
  font-size: ${fonts.ultra_large}px;
  padding-bottom: ${padding.medium}px;
  text-align: center;
`;

export const QuantityTitle = styled.span`
  width: 100%;
  color: ${colors.secondary};
  text-align: center;
`;

export const FilterTitle = styled.span`
  width: 100%;
  color: ${colors.secondary};
`;


export const Divider = styled.hr`
  width: 50%;
  align-self: center;
  margin: 30px auto;
  border: 0 none;
  border-top: 1px solid #DEDEDE;
`;
