import React, { useMemo } from 'react';
import { useSelector } from "react-redux";

import Item from './components/Item';

export default function List({
  ...props
}) {
  const jobs = useSelector(state => state.job.data);

  const jobsList = useMemo(() => {
    return jobs.map((job, index) => (
      <Item
        key={`job-item-${index}`}
        job={job}
      />
    ))

  }, [jobs]);

  return jobsList
}
