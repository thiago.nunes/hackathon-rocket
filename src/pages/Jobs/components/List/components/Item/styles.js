import styled from 'styled-components';

import { colors, padding } from "config";

export const Price = styled.span`
  width: 100%;
  color: ${colors.secondary};
  opacity: .5;
`;

export const Description = styled.span`
  width: 100%;
  color: ${colors.secondary};
  margin-top: ${padding.large}px;
`;

export const Skills = styled.div`
  width: 100%;
  display: flex;
  padding-top: ${padding.medium}px;
`;

export const Footer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  padding-top: ${padding.large}px;
`;
