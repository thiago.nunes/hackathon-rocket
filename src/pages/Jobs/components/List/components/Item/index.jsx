import React from 'react';
import { useDispatch } from "react-redux";

// Ducks
import { Actions } from 'ducks/modal';

// Components
import ModalCompany from "components/ModalCompany";
import Button from "components/Button";
import Panel from "components/Panel";
import Badge from "components/Badge";

// Styles
import {
  Price,
  Description,
  Skills,
  Footer,
} from './styles';

export default function Item({
  job,
  job: {
    title,
    priceRange,
    description,
    skills,
  },
}) {
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(Actions.openModal(() => <ModalCompany job={job} />))
  };

  return (
    <Panel
      title={title}
    >
      {/*<span style={{ width: '100%', color: '#fff' }}>{title}</span>*/}

      <Price>{priceRange}</Price>
      <Description>{description}</Description>
      {skills &&
        <Skills>
          {skills.map(skill => (
            <Badge key={skill} padding={"ultra_small"} fontSize={"ultra_small"}>{skill}</Badge>
          ))}
        </Skills>
      }
      <Footer>
        <Button onClick={handleClick}>
          Saiba mais
        </Button>
      </Footer>
    </Panel>
  )
}
