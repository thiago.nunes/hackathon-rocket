import styled from 'styled-components';
import { animation } from "../../config";
import { motion } from "framer-motion";

const slideAnimation = {
  hidden: {
    y: -300,
    opacity: 0,
  },
  visible: {
    y: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
      delay: .1,
      staggerChildren: .1,
      delayChildren: .5
    }
  },
};

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
`;

export const BoxLeft = styled(motion.div).attrs({
  initial: "hidden",
  animate: "visible",
  variants: slideAnimation
})`
  flex: 1 0 30%;
  padding-right: 20px;
`;

export const BoxRight = styled(motion.div).attrs({
  initial: "hidden",
  animate: "visible",
  variants: slideAnimation
})`
  flex: 1 0 70%;
`;

