import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";

// Components
import MainLayout from 'components/MainLayout'
import Filter from './components/Filter';
import List from './components/List';

// Styles
import {
  Wrapper,
  BoxLeft,
  BoxRight
} from './styles';

// Modules
import {
  Thunks
} from "ducks/job";

function Jobs() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(Thunks.getJobs());
  }, []);

  return (
    <MainLayout>
      <Wrapper>
        <BoxLeft>
          <Filter />
        </BoxLeft>
        <BoxRight>
          <List />
        </BoxRight>
      </Wrapper>
    </MainLayout>
  )
}

export default Jobs;
