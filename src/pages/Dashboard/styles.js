import styled from 'styled-components';
import { motion } from 'framer-motion';
import { animation } from "../../config";
import { Wrapper as BadgeWrapper } from 'components/Badge/styles';

const slideAnimation = {
  hidden: {
    y: -300,
    opacity: 0,
  },
  visible: {
    y: 0,
    opacity: 1,
    transition: {
      ...animation.spring,
      delay: .1,
      staggerChildren: .1,
      delayChildren: .5
    }
  },
};

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
`;

export const ListBadge = styled.div`
  margin-top: -10px;
  margin-left: -10px;
  
  ${BadgeWrapper} {
    padding-left: 0;
    margin-left: 10px;
    margin-top: 10px;
  }
`;

export const BoxLeft = styled(motion.div).attrs({
  initial: "hidden",
  animate: "visible",
  variants: slideAnimation
})`
  flex: 1 0 30%;
  padding-right: 20px;
`;

export const BoxRight = styled(motion.div).attrs({
  initial: "hidden",
  animate: "visible",
  variants: slideAnimation
})`
  flex: 1 0 70%;
`;
