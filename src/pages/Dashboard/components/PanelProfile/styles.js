import styled from 'styled-components';
import { Container as PanelContainer } from 'components/Panel/styles';
import { fonts } from 'config';

export const Wrapper = styled.div`
  color: #fff;
  text-align: center;
  
  ${PanelContainer} {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`

export const Content = styled.div`
  margin-top: 15px;
`

export const Title = styled.h1`
  font-size: ${fonts.large}px;
`

export const Text = styled.p`
  font-size: 16px;
`;

export const Divider = styled.hr`
  margin: 20px 0 10px;
  border: 0 none;
  border-top: 1px solid #DEDEDE;
`;

export const BigText = styled.h1`
  font-size: ${fonts.ultra_large}px;
`

export const Box = styled.div`
  & + & {
    margin-top: 15px;
  }
`