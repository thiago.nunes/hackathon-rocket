import React from 'react';
import Avatar from "components/Avatar";
import Panel from 'components/Panel'
import { Wrapper, Content, Title, Text, Divider, BigText, Box } from './styles'

function PanelProfile({  }) {
  return (
    <Wrapper>
      <Panel>
        <Avatar src="https://avatars1.githubusercontent.com/u/36231132?s=460&v=4" size={140} position="center" />
        <Content>
          <Title>Lucas Costa Amaral</Title>
          <Text>22 anos</Text>
          <Divider />
        </Content>
        <Box>
          <BigText>
            12
          </BigText>
          <Text>Visitas recentes</Text>
        </Box>
        <Box>
          <BigText>
            15
          </BigText>
          <Text>Conquistas adquiridas</Text>
        </Box>
      </Panel>
    </Wrapper>
  )
}

export default PanelProfile