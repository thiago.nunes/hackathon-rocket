import React from 'react';
import MainLayout from 'components/MainLayout'

// Teste
import Panel from 'components/Panel';
import Badge from "../../components/Badge";
import { Wrapper, BoxLeft, BoxRight, ListBadge } from "./styles";
import ListAchievement from 'components/ListAchievement';
import PanelProfile from './components/PanelProfile';

// Assets
import coding from './js.png';
import css from './css-3.svg';

function Dashboard() {
  return (
    <MainLayout>
      <Wrapper>
        <BoxLeft>
          <PanelProfile />
        </BoxLeft>
        <BoxRight>
          <Panel title="Conquistas">
            <ListBadge>
              <Badge>HTML - Formulários</Badge>
              <Badge>CSS - Flexbox</Badge>
              <Badge>CSS - Alinhamento de texto</Badge>
              <Badge>JS - Funções</Badge>
              <Badge>JS - Repetição WHILE</Badge>
              <Badge>JS - Recursividade</Badge>
            </ListBadge>
          </Panel>
          <Panel title="Consiga mais conquistas">
            <ListAchievement
              data={[
                { id: 'achievement1', image: coding, title: 'Batman SUMIU, bora salvar Gothan City', text: 'Java Script - Condicional IF' },
                // { id: 'achievement2', image: css, title: 'Ajude o naruto no seu treinamento de rasengan', text: 'CSS3' }
              ]}
            />
          </Panel>
        </BoxRight>
      </Wrapper>
    </MainLayout>
  )
}

export default Dashboard;
