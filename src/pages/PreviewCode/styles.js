import styled from 'styled-components';
import { colors } from 'config';

export const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  background-color: ${colors.secondary};
  color: ${colors.primary};
  display: flex;
  flex-direction: column;
`;

export const Content = styled.div`
  width: 100%;
  flex: 1
`;
