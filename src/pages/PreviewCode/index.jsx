import React from 'react';
import {
  Wrapper,
  Content
} from './styles';
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "ducks/alert";


function PreviewCode({ match: { params: { code } }}) {
  const dispatch = useDispatch();
  const alerts = useSelector(state => state.alert);

  window.alert = function() {
    dispatch(Actions.add(arguments[0]));
  };

  const decodedHTML = code ? atob(code) : '';

  return (
    <Wrapper>
      {alerts && alerts.map((message) => (
        <div key={message}>{message}</div>
      ))}
      <Content dangerouslySetInnerHTML={{ __html: decodedHTML }} />
    </Wrapper>
  )
}

export default PreviewCode;
