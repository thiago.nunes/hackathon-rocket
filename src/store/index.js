import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from 'ducks';

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;


export const store = createStore(
  reducers,
  // composeEnhancers(applyMiddleware(thunk))
  applyMiddleware(thunk)
);
