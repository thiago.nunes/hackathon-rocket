import { HTTP } from 'services';

export const fetchAll = () => HTTP.get(`/opportunity`);
export const fetchOne = (id) => HTTP.get(`/opportunity/${id}`);