import { HTTP } from 'services'

/**
 * Se candidata para uma vaga
 * @param userId
 * @param opportunityId
 */
export const applyJob = (userId, opportunityId) => (
  HTTP.post(`opportunities-users`, {
    user_id: userId,
    opportunity_id: opportunityId
  })
);