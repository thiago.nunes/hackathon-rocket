const jobs = [
  {
    title: 'Menor Aprendiz - Javascript',
    priceRange: 'R$ 500,00 - R$ 650,00',
    description: 'Descrição da empresa No mercado há mais de 60 anos, a Lello atua como líder de mercado no setor de imóveis e administração de condomínios. Com pé no futuro e de olho no desenvolvimento tecnológico trabalhamos para proporcionar aos nossos clientes a prestação de serviços de alta qua...',
    descriptionHTML: `
<p><strong>Descrição da empresa</strong></p>
<p>No mercado há mais de 60 anos, a Lello atua como líder de mercado no setor de imóveis e administração de condomínios. Com pé no futuro e de olho no desenvolvimento tecnológico trabalhamos para proporcionar aos nossos clientes a prestação de serviços de alta qualidade.</p>

<p><strong>Atividades e Responsabilidades</strong></p>
<ul>
  <li>Desenvolver as aplicações e sites requeridos pelo departamento, inovando os processos existentes na empresa e mantendo o bom funcionamento dos sites e sistemas.</li>
  <li>Assegurar o bom funcionamento do site da empresa.</li>
  <li>Buscar melhores implementações de funções já existentes tanto no site quanto em outros sistemas.</li>
</ul>

<p><strong>Requisitos</strong></p>
<ul>
  <li>Graduação em Tecnologia da Informação ou áreas correlatas.</li>
  <li>Necessário: Sólidos conhecimentos em Front End, Html / CSS, Bootstrap, Sass, Less, Scss, Responsive Web Design, Angular 2+, Conhecimento sólido em Javascript ES6 e Typescript, ReactJS, Familiaridade com REST APIs, Experiência com GIT</li>
  <li>Desejável: Ionic (mobile app framework), PWA (Progressive Web Apps)</li>
  <li>Foco na proposta de soluções;</li>
  <li>Vontade de impulsionar a empresa na transformação digital.</li>
</ul>

<p><strong>O que nós oferecemos</strong></p>
<ul>
  <li>Seguro de Vida</li>
  <li>Plano de Saúde</li>
  <li>Vale Transporte</li>
  <li>Vale Refeição</li>
</ul>`,
    skills: ['Condicional', 'Laço de repetição']
  },
  {
    title: 'Estágio - Javascript',
    priceRange: 'R$ 500,00 - R$ 650,00',
    description: 'Você gosta de tecnologia e quer novos desafios? queremos te conhecer! buscamos pessoas com sede de ...',
  },
  {
    title: 'Menor Aprendiz - HTML/CSS',
    priceRange: 'R$ 500,00 - R$ 650,00',
    description: 'Você gosta de tecnologia e quer novos desafios? queremos te conhecer! buscamos pessoas com sede de ...',
    skills: ['Lógica de programação']
  },
  {
    title: 'Menor Aprendiz - Javascript',
    priceRange: 'R$ 500,00 - R$ 650,00',
    description: 'Você gosta de tecnologia e quer novos desafios? queremos te conhecer! buscamos pessoas com sede de ...',
    skills: ['Laço de repetição', 'Funções']
  },
];

const initialValues = {
  data: jobs,
  loading: false,
};

export const Types = {
  REQUEST: 'hackathon/job/REQUEST',
  RECEIVE: 'hackathon/job/RECEIVE'
};

export const jobReducer = (state = initialValues, action) => {
  switch (action.type) {
    case Types.REQUEST:
      return {
        ...state,
        loading: false,
      };
    case Types.RECEIVE:
      return {
        ...state,
        data: action.payload.jobs,
        loading: false,
      };
    default: return state;
  }
};

export const Actions = {
  receiveJobs: (jobs) => ({ type: Types.RECEIVE, payload: { jobs } }),
  requestJobs: () => ({ type: Types.REQUEST }),
};

export const Thunks = {
  getJobs: () => dispatch => {
    dispatch(Actions.requestJobs());
    dispatch(Actions.receiveJobs(jobs));
  }
};
