import { combineReducers } from 'redux';

import { loaderReducer } from './loader';
import { jobReducer } from './job';
import { modalReducer } from './modal';
import { alertReducer } from './alert';

export default combineReducers({
  loader: loaderReducer,
  job: jobReducer,
  modal: modalReducer,
  alert: alertReducer,
});
