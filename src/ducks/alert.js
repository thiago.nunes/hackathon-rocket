const initialValues = [];

export const Types = {
  ADD: 'hackathon/alert/ADD',
};

export const alertReducer = (state = initialValues, action) => {
  switch (action.type) {
    case Types.ADD:
      return [
        ...state,
        action.payload.message,
      ];
    default: return state;
  }
};

export const Actions = {
  add: (message) => ({ type: Types.ADD, payload: { message } }),
};
