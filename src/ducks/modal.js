const initialValues = false;

export const Types = {
  SHOW: 'hackathon/modal/SHOW',
  HIDDEN: 'hackathon/modal/HIDDEN'
};

export const modalReducer = (state = initialValues, action) => {
  switch (action.type) {
    case Types.SHOW:
      return {
        ...state,
        component: action.payload.component,
        visible: true,
      };
    case Types.HIDDEN:
      return {
        ...state,
        component: null,
        visible: false,
      };
    default: return state;
  }
};

export const Actions = {
  openModal: (component) => ({ type: Types.SHOW, payload: { component } }),
  closeModal: () => ({ type: Types.HIDDEN }),
};
